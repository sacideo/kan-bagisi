<%--
  Created by IntelliJ IDEA.
  User: MTB
  Date: 4.04.2018
  Time: 00:06
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<tiles:importAttribute name="stylesheets"/>
<tiles:importAttribute name="javascripts"/>
<!DOCTYPE html>
<html lang="tr">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="${initParam.description}">
    <meta name="author" content="${initParam.author}">
    <link rel="Shortcut Icon" href="<c:url value="/WEB-INF/resources/vendor/jquery-ui/themes/flick/images/ui-icons_0073ea_256x240.png"/>" type="image/x-icon">

    <title><tiles:insertAttribute name="title"/></title>

    <c:forEach items="${stylesheets}" var="css">
        <link rel="stylesheet" type="text/css" href="<c:url value="${css}"/>">
    </c:forEach>

</head>
<body>

<div class="login-container">
    <div class="row">
        <div class="col-md-12">
            <div class="text-center m-b-md">
                <h3>Kullanıcı Giriş Ekranı</h3>
                <%--<small>This is the best app ever!</small>--%>
            </div>
            <div class="hpanel">
                <div class="panel-body">
                    <form action="<c:url value='login' />" name="login" method="post" id="loginForm">
                        <div class="form-group">
                            <label class="control-label" for="username">Kullanıcı Adı :</label>
                            <input type="text" placeholder="Example" title="Please enter you username" required="" value="" name="username" id="username" class="form-control">
                            <span class="help-block small"></span>
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="password">Kullanıcı Şifre :</label>
                            <input type="password" title="Please enter your password" placeholder="******" required="" value="" name="password" id="password" class="form-control">
                            <span class="help-block small"></span>
                        </div>
                        <%--<div class="checkbox">--%>
                            <%--<input type="checkbox" class="i-checks" checked>--%>
                            <%--Remember login--%>
                            <%--<p class="help-block small">(if this is a private computer)</p>--%>
                        <%--</div>--%>
                        <button class="btn btn-success btn-block">Giriş</button>
                        <%--<a class="btn btn-default btn-block" href="#">Kayıt Ol</a>--%>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 text-center">
            <strong>easycoPro</strong> Rezervasyon Sistemi <br/> 2018 © Copyright <a href="http://ibaysoft.com/">İBAYSOFT YAZILIM A.Ş</a>
        </div>
    </div>
</div>

<c:forEach items="${javascripts}" var="script">
    <script src="<c:url value="${script}"/>"></script>
</c:forEach>
</body>
</html>