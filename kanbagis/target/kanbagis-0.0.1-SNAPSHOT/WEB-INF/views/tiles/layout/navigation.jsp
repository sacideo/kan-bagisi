<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>


<nav role="navigation">
    <div class="header-link hide-menu"><i class="fa fa-bars"></i></div>
    <div class="small-logo">
        <span class="text-primary">easycoPro Rezervasyon Sistemleri</span>
    </div>
    <%--<form role="search" class="navbar-form-custom" method="post" action="#">--%>
    <%--<div class="form-group">--%>
    <%--<input type="text" placeholder="Search something special" class="form-control" name="search">--%>
    <%--</div>--%>
    <%--</form>--%>

    <div class="mobile-menu">
        <button type="button" class="navbar-toggle mobile-menu-toggle" data-toggle="collapse"
                data-target="#mobile-collapse">
            <i class="fa fa-chevron-down"></i>
        </button>
        <div class="collapse mobile-navbar" id="mobile-collapse">
            <ul class="nav navbar-nav">

                <%--<li>--%>
                <%--<a class="" href="profile.html">Profile</a>--%>
                <%--</li>--%>
                <li>
                    <a class="" href="<c:url value="/logout"/>">Logout</a>
                </li>
            </ul>
        </div>
    </div>

    <div class="navbar-right">
        <ul class="nav navbar-nav no-borders">
            <li class="dropdown">
                <a class="dropdown-toggle" href="#" data-toggle="dropdown">
                    <i class="pe-7s-speaker"></i>
                </a>
                <ul class="dropdown-menu hdropdown notification animated flipInX">
                    <li>
                        <a>
                            <span class="label label-success">NEW</span> It is a long established.
                        </a>
                    </li>
                    <li>
                        <a>
                            <span class="label label-warning">WAR</span> There are many variations.
                        </a>
                    </li>
                    <li>
                        <a>
                            <span class="label label-danger">ERR</span> Contrary to popular belief.
                        </a>
                    </li>
                    <li class="summary"><a href="#">See all notifications</a></li>
                </ul>
            </li>
            <li class="dropdown">
                <a class="dropdown-toggle" href="#" data-toggle="dropdown">
                    <i class="pe-7s-keypad"></i>
                </a>

                <div class="dropdown-menu hdropdown bigmenu animated flipInX">
                    <table>
                        <tbody>
                        <tr>
                            <td>
                                <a href="projects.html">
                                    <i class="pe pe-7s-portfolio text-info"></i>
                                    <h5>Projects</h5>
                                </a>
                            </td>
                            <td>
                                <a href="mailbox.html">
                                    <i class="pe pe-7s-mail text-warning"></i>
                                    <h5>Email</h5>
                                </a>
                            </td>
                            <td>
                                <a href="contacts.html">
                                    <i class="pe pe-7s-users text-success"></i>
                                    <h5>Contacts</h5>
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <a href="forum.html">
                                    <i class="pe pe-7s-comment text-info"></i>
                                    <h5>Forum</h5>
                                </a>
                            </td>
                            <td>
                                <a href="analytics.html">
                                    <i class="pe pe-7s-graph1 text-danger"></i>
                                    <h5>Analytics</h5>
                                </a>
                            </td>
                            <td>
                                <a href="file_manager.html">
                                    <i class="pe pe-7s-box1 text-success"></i>
                                    <h5>Files</h5>
                                </a>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </li>
            <li class="dropdown">
                <a class="dropdown-toggle label-menu-corner" href="#" data-toggle="dropdown">
                    <i class="pe-7s-mail"></i>
                    <span class="label label-success">4</span>
                </a>
                <ul class="dropdown-menu hdropdown animated flipInX">
                    <div class="title">
                        You have 4 new messages
                    </div>
                    <li>
                        <a>
                            It is a long established.
                        </a>
                    </li>
                    <li>
                        <a>
                            There are many variations.
                        </a>
                    </li>
                    <li>
                        <a>
                            Lorem Ipsum is simply dummy.
                        </a>
                    </li>
                    <li>
                        <a>
                            Contrary to popular belief.
                        </a>
                    </li>
                    <li class="summary"><a href="#">See All Messages</a></li>
                </ul>
            </li>
            <li>
                <a href="#" id="sidebar" class="right-sidebar-toggle">
                    <i class="pe-7s-upload pe-7s-news-paper"></i>
                </a>
            </li>
            <li class="dropdown">
                <a href="login.html">
                    <i class="pe-7s-upload pe-rotate-90"></i>
                </a>
            </li>
        </ul>
    </div>
</nav>
<!-- Navigation -->
<aside id="menu">
    <div id="navigation">
        <div class="profile-picture">
            <a href="<c:url value="/"/> ">
                <img src="<c:out value="/resources/images/profile.jpg"/>" class="img-circle m-b" alt="logo">
            </a>

            <div class="stats-label text-color">
                <span class="font-extra-bold font-uppercase">   <security:authentication
                        property="principal.userEntity.firstName"/>
            <security:authentication property="principal.userEntity.lastName"/></span>

                <div class="dropdown">
                    <a class="dropdown-toggle" href="#" data-toggle="dropdown">
                        <small class="text-muted">Kullanıcı İşlemleri <b class="caret"></b></small>
                    </a>
                    <ul class="dropdown-menu animated flipInX m-t-xs">
                        <%--<li><a href="profile.html">Profil</a></li>--%>
                        <li class="divider"></li>
                        <li><a href="<c:url value="${pageContext.request.contextPath}/logout"/> ">Çıkış</a></li>
                    </ul>
                </div>


                <%--<div id="sparkline1" class="small-chart m-t-sm"></div>--%>
                <%--<div>--%>
                <%--<h4 class="font-extra-bold m-b-xs">--%>
                <%--$260 104,200--%>
                <%--</h4>--%>
                <%--<small class="text-muted">Your income from the last year in sales product X.</small>--%>
                <%--</div>--%>
            </div>
        </div>

        <ul class="nav" id="side-menu">
            <li class="active">
                <a href="<c:url value="/"/> "> <span class="nav-label">Anasayfa</span> <span
                        class="label label-success pull-right">v.1</span> </a>
            </li>

            <security:authentication var="user" property="principal"/>
            <c:if test="${user.userEntity.userRole.userroleIsmi eq 'Admin' }">
                <li>
                    <a href="#"><span class="nav-label">Admin Paneli</span><span class="fa arrow"></span> </a>
                    <ul class="nav nav-second-level">
                        <li><a href="<c:url value="/"/>">Hastahane Tanımla</a></li>
                        <li><a href="<c:url value="/"/> ">İl Tanımla</a></li>
                        <li><a href="<c:url value="/"/> ">İlçe Tanımla</a></li>
                        <li><a href="<c:url value="/"/> ">Semt Tanımla</a></li>
                        <li><a href="<c:url value="/"/> ">Kan Gruplari Tanımla</a></li>
                        <li><a href="<c:url value="/"/> ">Kisi İletişim Tanımla</a></li>

                    </ul>
                </li>
                <%--<li>--%>
                <%--<a href="#"><span class="nav-label">Raporlar</span><span class="fa arrow"></span> </a>--%>
                <%--<ul class="nav nav-second-level">--%>
                <%--<li><a  href="#">RAPORLAR YAPILIYOR</a></li>--%>

                <%--</ul>--%>
                <%--</li>--%>
            </c:if>
            <c:if test="${user.userEntity.userRole.userroleIsmi eq 'User' || user.userEntity.userRole.userroleIsmi eq 'Admin' }">
                <li>
                    <a href="#"><span class="nav-label">Kan İşlemleri</span><span class="fa arrow"></span> </a>
                    <ul class="nav nav-second-level">
                        <li><a href="<c:url value="/"/> ">Kan Bağışçısı</a></li>
                    </ul>
                </li>
            </c:if>

            <%--TODO  3RD  YAZILIMCI ICIN ROLE YETKISI VERILECEK--%>
        </ul>
    </div>
</aside>