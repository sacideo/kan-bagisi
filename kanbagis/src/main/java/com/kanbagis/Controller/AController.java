package com.kanbagis.Controller;


import com.kanbagis.Domain.impl.Admin.Hastahaneler;
import com.kanbagis.Domain.impl.UserEntity;
import com.kanbagis.Service.impl.Admin.*;
import com.kanbagis.Service.impl.User.KanBagiscisiService;
import com.kanbagis.Service.impl.UserRoleService;
import com.kanbagis.Service.impl.UserService;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class AController {

    @Autowired
    protected UserService userService;
    @Autowired
    protected UserRoleService userRoleService;
    @Autowired
    protected HastahanelerServise hastahanelerServise;
    @Autowired
    protected IlcelerService ılcelerService;
    @Autowired
    protected IllerService ıllerService;
    @Autowired
    protected KanGruplariService kanGruplariService;
    @Autowired
    protected KisiIletisimService kisiIletisimService;
    @Autowired
    protected SemtlerService semtlerService;
    @Autowired
    protected KanBagiscisiService kanBagiscisiService;


    public boolean isRole(UserEntity user, String role) {
        if (user.getUserRole().getUserroleIsmi().matches(role)) {
            return true;
        } else {
            return false;
        }
    }

}
