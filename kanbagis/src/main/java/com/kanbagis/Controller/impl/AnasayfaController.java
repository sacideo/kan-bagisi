package com.kanbagis.Controller.impl;

import com.kanbagis.Controller.AController;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class AnasayfaController extends AController {


    @GetMapping(value = "/")
    public String getAnasayfa(ModelMap modelMap) {
        
        return "anasayfa";
    }


}
