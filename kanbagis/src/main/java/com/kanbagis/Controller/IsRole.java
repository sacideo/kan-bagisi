package com.kanbagis.Controller;

public enum IsRole {
    Admin {
        @Override
        public String toString() {
            return "Admin";
        }
    },
    User {
        @Override
        public String toString() {
            return "User";
        }
    }
}
