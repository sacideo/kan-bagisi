package com.kanbagis.Domain;

import com.kanbagis.Domain.impl.UserEntity;
import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;

import javax.persistence.*;
import java.util.Date;

@MappedSuperclass
public abstract class ADomain implements IGenericDomain {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", nullable = false)
    private Integer id;


    @Column(name = "DURUM", columnDefinition = "int default 1")
    private Integer durum;

    @Column(name = "ACIKLAMA", length = 255)
    private String aciklamasi;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CREATEDATE")
    @Generated(GenerationTime.INSERT)
    private Date createDate;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "UPDATEDATE")
    @Generated(GenerationTime.ALWAYS)
    private Date updateDate;

    public ADomain() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


    public Integer getDurum() {
        return durum;
    }

    public void setDurum(Integer durum) {
        this.durum = durum;
    }

    public String getAciklamasi() {
        return aciklamasi;
    }

    public void setAciklamasi(String aciklamasi) {
        this.aciklamasi = aciklamasi;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    @Override
    public int getID() {
        return id;
    }
}
