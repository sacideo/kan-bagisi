package com.kanbagis.Domain.impl.Admin;

import com.kanbagis.Domain.ADomain;
import com.kanbagis.Domain.impl.UserEntity;

import javax.persistence.*;

@Entity
@Table(name = "KisiIletisim_TB")
public class KisiIletisim extends ADomain {

    @Column(name = "ADRESEK")
    private String adresEk;
    @Column(name = "TELEFONNUMARASI")
    private String telefonNumarasi;
    @Column(name = "EMAIL")
    private String eMail;

    @ManyToOne
    @JoinColumn(name = "USER")
    private UserEntity userEntity;

    @ManyToOne
    @JoinColumn(name = "IL")
    private Iller iller;
    @ManyToOne
    @JoinColumn(name = "ILCELER")
    private Ilceler ilceler;
    @ManyToOne
    @JoinColumn(name = "SEMTLER")
    private Semtler semtler;

    public String getAdresEk() {
        return adresEk;
    }

    public void setAdresEk(String adresEk) {
        this.adresEk = adresEk;
    }

    public String getTelefonNumarasi() {
        return telefonNumarasi;
    }

    public void setTelefonNumarasi(String telefonNumarasi) {
        this.telefonNumarasi = telefonNumarasi;
    }

    public String geteMail() {
        return eMail;
    }

    public void seteMail(String eMail) {
        this.eMail = eMail;
    }

    public UserEntity getUserEntity() {
        return userEntity;
    }

    public void setUserEntity(UserEntity userEntity) {
        this.userEntity = userEntity;
    }

    public Iller getIller() {
        return iller;
    }

    public void setIller(Iller iller) {
        this.iller = iller;
    }

    public Ilceler getIlceler() {
        return ilceler;
    }

    public void setIlceler(Ilceler ilceler) {
        this.ilceler = ilceler;
    }

    public Semtler getSemtler() {
        return semtler;
    }

    public void setSemtler(Semtler semtler) {
        this.semtler = semtler;
    }
}
