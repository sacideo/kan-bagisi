package com.kanbagis.Domain.impl.Admin;

import com.kanbagis.Domain.ADomain;
import com.kanbagis.Domain.impl.UserEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;

@Entity
@Table(name = "Kangruplari_TB")
public class KanGruplari extends ADomain {

    @Column(name = "KANGRUBUISMI")
    private String kangrubuIsmi;
    @OneToMany(mappedBy = "kanGruplari")
    private List<UserEntity> userEntities;

    public KanGruplari() {
    }

    public String getKangrubuIsmi() {
        return kangrubuIsmi;
    }

    public List<UserEntity> getUserEntities() {
        return userEntities;
    }

    public void setUserEntities(List<UserEntity> userEntities) {
        this.userEntities = userEntities;
    }

    public void setKangrubuIsmi(String kangrubuIsmi) {
        this.kangrubuIsmi = kangrubuIsmi;
    }
}
