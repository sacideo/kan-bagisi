package com.kanbagis.Domain.impl;


import com.kanbagis.Domain.IGenericDomain;

import javax.persistence.*;

@Entity
@Table(name = "USERROLE_TB")
public class UserRole implements IGenericDomain {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int userroleId;
    @Column(name = "USERROLEISMI", length = 255)
    private String userroleIsmi;

    public String getUserroleIsmi() {
        return userroleIsmi;
    }

    public void setUserroleIsmi(String userroleIsmi) {
        this.userroleIsmi = userroleIsmi;
    }

    @Override
    public int getID() {
        return userroleId;
    }
}
