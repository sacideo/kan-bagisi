package com.kanbagis.Domain.impl.Admin;

import com.kanbagis.Domain.ADomain;

import javax.persistence.*;

@Entity
@Table(name = "Semtler_TB")
public class Semtler extends ADomain {

    @Column(name = "SEMTLER")
    private String semtler;

    @ManyToOne
    @JoinColumn(name = "Ilceler_Id")
    private Ilceler ılceler;

    public Semtler() {
    }

    public String getSemtler() {
        return semtler;
    }

    public void setSemtler(String semtler) {
        this.semtler = semtler;
    }

    public Ilceler getIlceler() {
        return ılceler;
    }

    public void setIlceler(Ilceler ılceler) {
        this.ılceler = ılceler;
    }
}
