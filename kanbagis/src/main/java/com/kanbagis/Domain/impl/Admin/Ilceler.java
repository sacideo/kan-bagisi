package com.kanbagis.Domain.impl.Admin;

import com.kanbagis.Domain.ADomain;

import javax.persistence.*;

@Entity
@Table(name = "Ilceler_TB")
public class Ilceler extends ADomain {

    @Column(name = "ILCELER")
    private String ilceler;
    @ManyToOne
    @JoinColumn(name = "Iller_Id")
    private Iller ıller;

    public Ilceler() {
    }

    public Iller getIller() {
        return ıller;
    }

    public void setIller(Iller ıller) {
        this.ıller = ıller;
    }

    public String getIlceler() {
        return ilceler;
    }

    public void setIlceler(String ilceler) {
        this.ilceler = ilceler;
    }
}
