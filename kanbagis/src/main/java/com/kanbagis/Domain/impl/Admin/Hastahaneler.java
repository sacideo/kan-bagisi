package com.kanbagis.Domain.impl.Admin;

import com.kanbagis.Domain.ADomain;
import com.kanbagis.Domain.impl.UserEntity;

import javax.persistence.*;

@Entity
@Table(name = "Hastahane_TB")
public class Hastahaneler extends ADomain {

    @Column(name = "HASTAHANEISMI")
    private String hastahaneIsmi;
    @Column(name = "HASTAHANEADRESEK")
    private String hastahaneAdresEk;
    @Column(name = "HASTAHANETELEFON")
    private String hastahaneTelefon;

    @ManyToOne
    @JoinColumn(name = "HASTAHANEIL")
    private Iller iller;

    @ManyToOne
    @JoinColumn(name = "HASTAHANEILCE")
    private Ilceler ilceler;

    @ManyToOne
    @JoinColumn(name = "HASTAHANESEMT")
    private Semtler semtler;

    @ManyToOne
    @JoinColumn(name = "HASTAHANEBASHEKIM")
    private UserEntity userEntity;


    public Hastahaneler() {
    }

    public String getHastahaneIsmi() {
        return hastahaneIsmi;
    }

    public void setHastahaneIsmi(String hastahaneIsmi) {
        this.hastahaneIsmi = hastahaneIsmi;
    }

    public String getHastahaneAdresEk() {
        return hastahaneAdresEk;
    }

    public void setHastahaneAdresEk(String hastahaneAdresEk) {
        this.hastahaneAdresEk = hastahaneAdresEk;
    }

    public String getHastahaneTelefon() {
        return hastahaneTelefon;
    }

    public void setHastahaneTelefon(String hastahaneTelefon) {
        this.hastahaneTelefon = hastahaneTelefon;
    }

    public Iller getIller() {
        return iller;
    }

    public void setIller(Iller iller) {
        this.iller = iller;
    }

    public Ilceler getIlceler() {
        return ilceler;
    }

    public void setIlceler(Ilceler ilceler) {
        this.ilceler = ilceler;
    }

    public Semtler getSemtler() {
        return semtler;
    }

    public void setSemtler(Semtler semtler) {
        this.semtler = semtler;
    }

    public UserEntity getUserEntity() {
        return userEntity;
    }

    public void setUserEntity(UserEntity userEntity) {
        this.userEntity = userEntity;
    }
}
