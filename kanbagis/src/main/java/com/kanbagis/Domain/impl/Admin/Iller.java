package com.kanbagis.Domain.impl.Admin;

import com.kanbagis.Domain.ADomain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;

@Entity
@Table(name = "Iller_TB")
public class Iller extends ADomain {

    @Column(name = "ILLER")
    private String ilAdi;

    public Iller() {
    }

    public String getIlAdi() {
        return ilAdi;
    }

    public void setIlAdi(String ilAdi) {
        this.ilAdi = ilAdi;
    }
}
