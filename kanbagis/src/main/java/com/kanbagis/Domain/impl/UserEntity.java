package com.kanbagis.Domain.impl;


import com.kanbagis.Domain.IGenericDomain;
import com.kanbagis.Domain.impl.Admin.KanGruplari;

import javax.persistence.*;

@Entity
@Table(name = "USER_TB")
public class UserEntity implements IGenericDomain {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int  userId;
    @Column(name = "FIRSTNAME", length = 255)
    private String firstName;
    @Column(name = "LASTNAME", length = 255)
    private String lastName;
    @Column(name = "USERNAME", length = 255, unique = true)
    private String userName;
    @Column(name = "USERPASSWORD", length = 255, unique = true)
    private String userPassword;
    @Column(name = "USEREMAIL", length = 100, unique = true)
    private String userEmail;
    @Column(name = "USERPHONE", length = 10)
    private String userPhone;
    @ManyToOne
    @JoinColumn(name = "USERROLEID")
    private UserRole userRole;
    @ManyToOne
    @JoinColumn
    private KanGruplari kanGruplari;


    public int getUserId() {
        return userId;
    }

    public KanGruplari getKanGruplari() {
        return kanGruplari;
    }

    public void setKanGruplari(KanGruplari kanGruplari) {
        this.kanGruplari = kanGruplari;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public UserRole getUserRole() {
        return userRole;
    }

    public void setUserRole(UserRole userRole) {
        this.userRole = userRole;
    }

    @Override
    public int getID() {
        return userId;
    }
}
