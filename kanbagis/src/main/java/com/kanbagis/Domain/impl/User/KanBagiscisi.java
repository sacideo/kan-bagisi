package com.kanbagis.Domain.impl.User;

import com.kanbagis.Domain.ADomain;
import com.kanbagis.Domain.impl.UserEntity;

import javax.persistence.*;

@Entity
@Table(name = "Kanbagiscisi_TB")
public class KanBagiscisi extends ADomain {

    @ManyToOne
    @JoinColumn(name = "UserID")
    private UserEntity userEntity;
    
}
