package com.kanbagis.Domain;

import java.io.Serializable;

public interface IGenericDomain extends Serializable {
    public abstract int getID();
}
