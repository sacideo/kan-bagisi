package com.kanbagis.Repository;



import com.kanbagis.Domain.IGenericDomain;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Criterion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
public abstract class ARepository<ENTITY extends IGenericDomain> implements IGenericRepository<ENTITY> {
    @Autowired
    protected SessionFactory sessionFactory;

    private final Class<ENTITY> entityClass;

    public ARepository(Class<ENTITY> entityClass) {
        this.entityClass = entityClass;
    }

    @Override
    public void save(ENTITY entity) {
        sessionFactory.getCurrentSession().persist(entity);
    }

    @Override
    public void update(ENTITY entity) {
        sessionFactory.getCurrentSession().update(entity);
    }

    @Override
    public void delete(ENTITY entity) {
        sessionFactory.getCurrentSession().delete(entity);
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<ENTITY> getAll() {
        return sessionFactory.getCurrentSession().createCriteria(entityClass).list();
    }

    @Override
    public ENTITY getById(Integer id) {
        return ((ENTITY) sessionFactory.getCurrentSession().get(entityClass, id));
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<ENTITY> findByCriteria(Criterion... criterion) {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(entityClass);
        for (Criterion c : criterion) {
            criteria.add(c);
        }
        return criteria.list();
    }

}
