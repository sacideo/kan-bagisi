package com.kanbagis.Repository.impl.Admin;

import com.kanbagis.Domain.impl.Admin.KanGruplari;
import com.kanbagis.Repository.ARepository;
import org.springframework.stereotype.Repository;

@Repository("kangruplarirepo")
public class KanGruplariRepo extends ARepository<KanGruplari> {
    public KanGruplariRepo() {
        super(KanGruplari.class);
    }
}
