package com.kanbagis.Repository.impl;


import com.kanbagis.Domain.impl.UserEntity;
import com.kanbagis.Repository.ARepository;
import org.springframework.stereotype.Repository;

@Repository("userrepo")
public class UserRepo extends ARepository<UserEntity> {


    public UserRepo() {
        super(UserEntity.class);
    }
}
