package com.kanbagis.Repository.impl.Admin;

import com.kanbagis.Domain.impl.Admin.KisiIletisim;
import com.kanbagis.Repository.ARepository;
import org.springframework.stereotype.Repository;

@Repository("kisiiletisimrepo")
public class KisiIletisimRepo extends ARepository<KisiIletisim> {
    public KisiIletisimRepo() {
        super(KisiIletisim.class);
    }
}
