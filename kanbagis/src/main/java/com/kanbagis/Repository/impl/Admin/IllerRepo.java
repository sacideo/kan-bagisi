package com.kanbagis.Repository.impl.Admin;

import com.kanbagis.Domain.impl.Admin.Iller;
import com.kanbagis.Repository.ARepository;
import org.springframework.stereotype.Repository;

@Repository("illerrepo")
public class IllerRepo extends ARepository<Iller> {
    public IllerRepo() {
        super(Iller.class);
    }
}
