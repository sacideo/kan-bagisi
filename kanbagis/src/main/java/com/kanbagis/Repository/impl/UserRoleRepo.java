package com.kanbagis.Repository.impl;

import com.kanbagis.Domain.impl.UserRole;
import com.kanbagis.Repository.ARepository;
import org.springframework.stereotype.Repository;

@Repository("UserRoleRepo")
public class UserRoleRepo extends ARepository<UserRole> {
    public UserRoleRepo() {
        super(UserRole.class);
    }
}
