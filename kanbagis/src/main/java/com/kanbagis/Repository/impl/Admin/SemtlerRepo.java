package com.kanbagis.Repository.impl.Admin;

import com.kanbagis.Domain.impl.Admin.Semtler;
import com.kanbagis.Repository.ARepository;
import org.springframework.stereotype.Repository;

@Repository("semtler")
public class SemtlerRepo extends ARepository<Semtler> {
    public SemtlerRepo() {
        super(Semtler.class);
    }
}
