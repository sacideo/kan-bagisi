package com.kanbagis.Repository.impl.Admin;

import com.kanbagis.Domain.ADomain;
import com.kanbagis.Domain.impl.Admin.Ilceler;
import com.kanbagis.Repository.ARepository;
import org.springframework.stereotype.Repository;

@Repository("ilcelerrepo")
public class IlcelerRepo extends ARepository<Ilceler> {
    public IlcelerRepo() {
        super(Ilceler.class);
    }
}
