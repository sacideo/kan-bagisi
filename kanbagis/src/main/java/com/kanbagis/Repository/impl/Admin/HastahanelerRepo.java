package com.kanbagis.Repository.impl.Admin;

import com.kanbagis.Domain.impl.Admin.Hastahaneler;
import com.kanbagis.Repository.ARepository;
import org.springframework.stereotype.Repository;

@Repository("hastahanerepo")
public class HastahanelerRepo extends ARepository<Hastahaneler> {


    public HastahanelerRepo() {
        super(Hastahaneler.class);
    }
}
