package com.kanbagis.Repository.impl.User;

import com.kanbagis.Domain.impl.User.KanBagiscisi;
import com.kanbagis.Repository.ARepository;
import org.springframework.stereotype.Repository;

@Repository("kanbagiscisirepo")
public class KanBagiscisiRepo extends ARepository<KanBagiscisi> {
    public KanBagiscisiRepo() {
        super(KanBagiscisi.class);
    }
}
