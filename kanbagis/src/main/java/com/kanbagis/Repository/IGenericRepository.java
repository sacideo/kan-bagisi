package com.kanbagis.Repository;



import com.kanbagis.Domain.IGenericDomain;
import org.hibernate.criterion.Criterion;

import java.util.List;

public interface IGenericRepository<ENTITY extends IGenericDomain> {

    public abstract void save(ENTITY entity);

    public abstract void update(ENTITY entity);

    public abstract void delete(ENTITY entity);

    public abstract List<ENTITY> getAll();

    public abstract ENTITY getById(Integer id);

    public abstract List<ENTITY> findByCriteria(Criterion... criterion);

}
