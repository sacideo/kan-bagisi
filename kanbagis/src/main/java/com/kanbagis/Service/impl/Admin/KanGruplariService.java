package com.kanbagis.Service.impl.Admin;

import com.kanbagis.Domain.impl.Admin.KanGruplari;
import com.kanbagis.Repository.ARepository;
import com.kanbagis.Service.AService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class KanGruplariService extends AService<KanGruplari> {
    public KanGruplariService(@Qualifier("kangruplarirepo") ARepository<KanGruplari> genericRepo) {
        super(genericRepo);
    }
}
