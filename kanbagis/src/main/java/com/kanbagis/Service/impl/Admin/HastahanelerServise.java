package com.kanbagis.Service.impl.Admin;

import com.kanbagis.Domain.impl.Admin.Hastahaneler;
import com.kanbagis.Repository.ARepository;
import com.kanbagis.Service.AService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class HastahanelerServise extends AService<Hastahaneler> {
    @Autowired
    public HastahanelerServise(@Qualifier("hastahanerepo") ARepository<Hastahaneler> genericRepo) {
        super(genericRepo);
    }
}
