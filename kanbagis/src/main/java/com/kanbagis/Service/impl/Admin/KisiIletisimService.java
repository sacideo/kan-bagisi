package com.kanbagis.Service.impl.Admin;

import com.kanbagis.Domain.impl.Admin.KisiIletisim;
import com.kanbagis.Repository.ARepository;
import com.kanbagis.Service.AService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class KisiIletisimService extends AService<KisiIletisim> {
    public KisiIletisimService(@Qualifier("kisiiletisimrepo") ARepository<KisiIletisim> genericRepo) {
        super(genericRepo);
    }
}
