package com.kanbagis.Service.impl;


import com.kanbagis.Domain.impl.UserEntity;
import com.kanbagis.Repository.ARepository;
import com.kanbagis.Repository.impl.UserRepo;
import com.kanbagis.Service.AService;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService extends AService<UserEntity> {

    @Autowired
    public UserService(@Qualifier("userrepo") ARepository<UserEntity> genericRepo) {
        super(genericRepo);
    }
    public UserEntity getByUserName(String userName) {
        List<UserEntity> users = ((UserRepo) getGenericRepo()).findByCriteria(Restrictions.eq("userName", userName));
        return users.size() > 0 ? users.get(0) : null;
    }
}
