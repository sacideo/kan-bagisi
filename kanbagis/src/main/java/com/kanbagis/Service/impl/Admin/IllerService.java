package com.kanbagis.Service.impl.Admin;

import com.kanbagis.Domain.impl.Admin.Iller;
import com.kanbagis.Repository.ARepository;
import com.kanbagis.Service.AService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class IllerService extends AService<Iller> {
    @Autowired
    public IllerService(@Qualifier("illerrepo") ARepository<Iller> genericRepo) {
        super(genericRepo);
    }
}
