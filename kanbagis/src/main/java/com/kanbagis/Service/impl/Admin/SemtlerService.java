package com.kanbagis.Service.impl.Admin;

import com.kanbagis.Domain.impl.Admin.Semtler;
import com.kanbagis.Repository.ARepository;
import com.kanbagis.Service.AService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class SemtlerService extends AService<Semtler> {
    public SemtlerService(@Qualifier("semtler") ARepository<Semtler> genericRepo) {
        super(genericRepo);
    }
}
