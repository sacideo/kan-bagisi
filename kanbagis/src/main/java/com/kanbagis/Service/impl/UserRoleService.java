package com.kanbagis.Service.impl;

import com.kanbagis.Domain.impl.UserRole;
import com.kanbagis.Repository.ARepository;
import com.kanbagis.Service.AService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class UserRoleService extends AService<UserRole> {
    @Autowired
    public UserRoleService(@Qualifier("UserRoleRepo") ARepository<UserRole> genericRepo) {
        super(genericRepo);
    }
}
