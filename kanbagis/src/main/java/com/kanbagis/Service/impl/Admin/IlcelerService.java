package com.kanbagis.Service.impl.Admin;

import com.kanbagis.Domain.impl.Admin.Ilceler;
import com.kanbagis.Repository.ARepository;
import com.kanbagis.Service.AService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class IlcelerService extends AService<Ilceler> {
    @Autowired
    public IlcelerService(@Qualifier("ilcelerrepo") ARepository<Ilceler> genericRepo) {
        super(genericRepo);
    }
}
