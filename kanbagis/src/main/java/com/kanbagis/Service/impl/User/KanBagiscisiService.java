package com.kanbagis.Service.impl.User;

import com.kanbagis.Domain.impl.User.KanBagiscisi;
import com.kanbagis.Repository.ARepository;
import com.kanbagis.Service.AService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class KanBagiscisiService extends AService<KanBagiscisi> {
    public KanBagiscisiService(@Qualifier("kanbagiscisirepo") ARepository<KanBagiscisi> genericRepo) {
        super(genericRepo);
    }
}
