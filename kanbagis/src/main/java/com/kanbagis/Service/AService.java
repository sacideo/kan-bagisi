package com.kanbagis.Service;





import com.kanbagis.Domain.IGenericDomain;
import com.kanbagis.Repository.ARepository;

import java.util.List;


public abstract class AService<ENTITY extends IGenericDomain> implements IGenericService<ENTITY> {

    private final ARepository<ENTITY> genericRepo;

    public AService(ARepository<ENTITY> genericRepo) {
        this.genericRepo = genericRepo;
    }

    public ARepository<ENTITY> getGenericRepo() {
        return genericRepo;
    }

    @Override
    public void save(ENTITY entity) {
        genericRepo.save(entity);
    }

    @Override
    public void update(ENTITY entity) {
        genericRepo.update(entity);
    }

    @Override
    public void delete(ENTITY entity) {
        genericRepo.delete(entity);
    }

    @Override
    public List<ENTITY> getAll() {
        return genericRepo.getAll();
    }

    @Override
    public ENTITY getById(Integer id) {
        return genericRepo.getById(id);
    }

}
