package com.kanbagis.Service;



import com.kanbagis.Domain.IGenericDomain;

import java.util.List;

public interface IGenericService<ENTITY extends IGenericDomain> {

    public abstract void save(ENTITY entity);

    public abstract void update(ENTITY entity);

    public abstract void delete(ENTITY entity);

    public abstract List<ENTITY> getAll();

    public abstract ENTITY getById(Integer id);
}
