package com.kanbagis.Security;



import com.kanbagis.Domain.impl.UserEntity;
import com.kanbagis.Service.impl.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component("customUserDetailsService")
public class CustomUserDetailService implements UserDetailsService {

    @Autowired
    private UserService userService;

    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
        CustomUserDetail user = null;
        UserEntity userEntity = userService.getByUserName(userName);
        if (userEntity == null) {
            throw new UsernameNotFoundException("Username not found");
        } else {
            SimpleGrantedAuthority grantedAuth = new SimpleGrantedAuthority(userEntity.getUserRole().getUserroleIsmi());
            List<GrantedAuthority> grantedAuthoritieList = new ArrayList<GrantedAuthority>();
            grantedAuthoritieList.add(grantedAuth);
            user = new CustomUserDetail(userEntity.getUserName(), userEntity.getUserPassword(), userEntity, grantedAuthoritieList);
        }
        return user;
    }

}
