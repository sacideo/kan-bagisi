package com.kanbagis.Security;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;


public final class SecurityUtil {

	private SecurityUtil() {
	}

	public static CustomUserDetail getAuthenticatedUser() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		return (CustomUserDetail) authentication.getPrincipal();
	}
}
