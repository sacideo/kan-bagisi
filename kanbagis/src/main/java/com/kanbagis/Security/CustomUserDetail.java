package com.kanbagis.Security;



import com.kanbagis.Domain.impl.UserEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.Collection;

/**
 * 
 * @author MTB
 * @version 1.0.0
 */
public class CustomUserDetail extends User {

 	private UserEntity userEntity;

	public CustomUserDetail(String username, String password,UserEntity userEntity, Collection<? extends GrantedAuthority> authorities) {
		super(username, password, authorities);
		this.userEntity = userEntity;
	}

	public UserEntity getUserEntity() {
		return userEntity;
	}

	public void setUserEntity(UserEntity userEntity) {
		this.userEntity = userEntity;
	}
	//




}
