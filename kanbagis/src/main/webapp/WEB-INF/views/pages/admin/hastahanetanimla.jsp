
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Hastahane</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
  <h2>Hastahane Tanımla</h2>
  <form class="form-horizontal" action="">
    <div class="form-group">
      <label class="control-label col-sm-2" for="name">Hastahane İsmi:</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" id="name" placeholder="Hastahane İsmi" name="name">
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-2" for="adress">Adres:</label>
      <div class="col-sm-10">          
        <input type="text" class="form-control" id="adress" placeholder="Adres" name="adress">
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-2" for="tel">Telefon:</label>
      <div class="col-sm-10">          
        <input type="number" class="form-control" id="tel" placeholder="Telefon" name="tel">
      </div>
    </div>
    <div class="form-group">        
      <div class="col-sm-offset-2 col-sm-10">
        <button type="submit" class="btn btn-default">Kaydet</button>
      </div>
    </div>
  </form>
</div>
</body>
</html>


