<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<tiles:importAttribute name="stylesheets" />
<tiles:importAttribute name="javascripts" />
<!DOCTYPE html>
<html lang="tr">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="${initParam.description}">
    <meta name="author" content="${initParam.author}">
    <link rel="Shortcut Icon"  href="<c:url value="/WEB-INF/resources/images/a1.jpg"/>"  type="image/x-icon">
    <!-- Extra styles from tiles -->
    <title><tiles:insertAttribute name="title" /></title>
    <!-- Styles -->
    <c:forEach items="${stylesheets}" var="css">
        <c:choose>
            <c:when test="${fn:startsWith(css, '/resources')}">
                <link rel="stylesheet" type="text/css" href="<c:url value="${css}"/>">
            </c:when>
            <c:otherwise>
                <link rel="stylesheet" type="text/css" href="${css}"/>
            </c:otherwise>
        </c:choose>
    </c:forEach>
</head>
<script>
    var ctx = "${pageContext.request.contextPath}";
</script>
<body>
<div class="splash"> <div class="color-line"></div><div class="splash-title"><h1>Homer - Responsive Admin Theme</h1><p>Special Admin Theme for small and medium webapp with very clean and aesthetic style and feel. </p><div class="spinner"> <div class="rect1"></div> <div class="rect2"></div> <div class="rect3"></div> <div class="rect4"></div> <div class="rect5"></div> </div> </div> </div>
<!--[if lt IE 7]>
<p class="alert alert-danger">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

<!-- Header -->
<div id="header">
    <div class="color-line">
    </div>
    <div id="logo" class="light-version">
        <span>
            Homer Theme
        </span>
    </div>
    <tiles:insertAttribute name="navigation" />
</div>
<div id="wrapper">

    <div id="content animate-panel">
        <div class="row">
            <div class="col-lg-12 text-center m-t-md">
                <h2>
                    <tiles:insertAttribute name="heading" />
                </h2>
            </div>
        </div>
        <tiles:insertAttribute name="content" />
    </div>
</div>

<!-- Javascripts -->
<c:forEach items="${javascripts}" var="script">
    <c:choose>
        <c:when test="${fn:startsWith(script, '/resources')}">
            <script type="text/javascript" src="<c:url value="${script}"/>"></script>
        </c:when>
        <c:otherwise>
            <script type="text/javascript" src="${script}"></script>
        </c:otherwise>
    </c:choose>
</c:forEach>
</body>
</html>